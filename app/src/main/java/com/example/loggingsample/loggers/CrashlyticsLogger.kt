package com.example.loggingsample.loggers

import android.util.Log
import com.crashlytics.android.Crashlytics
import timber.log.Timber

class CrashlyticsLogger : Timber.Tree() {
    override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
        if(priority >= Log.WARN) {
            Crashlytics.logException(t ?: Exception(message))
        }
        Crashlytics.log(priority, tag, message)
    }
}