package com.example.loggingsample.loggers.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.loggingsample.loggers.DatabaseLogger
import org.koin.dsl.module

val logsDbModule = module {
    single { Room.databaseBuilder(get(), LogsDatabase::class.java, "logs-database").build() }
    single { get<LogsDatabase>().dao() }
    single { DatabaseLogger(get()) }
}

@Dao
interface LogsDao {
    @Insert
    suspend fun inserLog(entry: LogEntry)

    @Query("SELECT * FROM logentry LIMIT 10")
    fun logs() : LiveData<List<LogEntry>>
}

@Database(entities = arrayOf(LogEntry::class), version = 1)
abstract class LogsDatabase: RoomDatabase() {
    abstract fun dao(): LogsDao
}
