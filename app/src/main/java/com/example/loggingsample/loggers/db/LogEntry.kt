package com.example.loggingsample.loggers.db

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class LogEntry(
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0,
    val tag: String?,
    val message: String
)