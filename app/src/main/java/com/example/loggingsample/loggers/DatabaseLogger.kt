package com.example.loggingsample.loggers

import com.example.loggingsample.loggers.db.LogEntry
import com.example.loggingsample.loggers.db.LogsDao
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import timber.log.Timber

class DatabaseLogger(val logsDao: LogsDao) : Timber.Tree() {
    override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
        GlobalScope.launch(Dispatchers.IO) {
            logsDao.inserLog(LogEntry(tag = tag, message = message))
        }
    }
}