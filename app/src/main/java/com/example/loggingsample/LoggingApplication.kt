package com.example.loggingsample

import android.app.Application
import android.content.Intent
import com.example.loggingsample.loggers.CrashlyticsLogger
import com.example.loggingsample.loggers.DatabaseLogger
import com.example.loggingsample.loggers.db.logsDbModule
import org.koin.android.ext.android.inject
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber

class LoggingApplication : Application() {

    private val databaseLogger: DatabaseLogger by inject()

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@LoggingApplication)
            modules(logsDbModule)
        }

        Timber.plant(CrashlyticsLogger())
        Timber.plant(databaseLogger)

        Intent(this, LogNotificationService::class.java).also {
            startService(it)
        }

    }
}