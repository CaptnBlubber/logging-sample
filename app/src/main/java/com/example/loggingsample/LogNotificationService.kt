package com.example.loggingsample

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.lifecycle.LifecycleService
import androidx.lifecycle.Observer
import com.example.loggingsample.loggers.db.LogsDao
import org.koin.android.ext.android.inject

class LogNotificationService : LifecycleService() {

    private val logsDao: LogsDao by inject()

    private val notifiedLogs = mutableListOf<Long>()

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        createNotificationChannel()
        logsDao.logs().observe(this, Observer { logs ->
            NotificationManagerCompat.from(this).apply {
                logs.filter { !notifiedLogs.contains(it.id) }.forEach { logEntry ->
                    val logBuilder = NotificationCompat.Builder(this@LogNotificationService, CHANNEL_ID)
                        .setSmallIcon(R.drawable.ic_launcher_foreground)
                        .setContentTitle(logEntry.tag ?: "Unkown")
                        .setContentText(logEntry.message)
                        .setPriority(NotificationCompat.PRIORITY_HIGH)
                    notify(logEntry.id.toInt(), logBuilder.build())
                    notifiedLogs.add(logEntry.id)
                }
            }
        })

        return super.onStartCommand(intent, flags, startId)
    }

    private fun createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            val importance = NotificationManager.IMPORTANCE_HIGH
            val channel = NotificationChannel(CHANNEL_ID, "Logging Sampe", importance).apply {
                description = "This channel is used to show log messages"
            }
            // Register the channel with the system
            val notificationManager: NotificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }

    companion object {
        private const val GROUP_NOTIFICATION_ID = 1337
        private const val CHANNEL_ID = "Sample Logger"
        private const val GROUP_KEY = "Logs"
    }
}
